//
//  ViewController.swift
//  ExploringColorPicker
//
//  Created by Jeffry Sandy Purnomo on 29/03/21.
//

import UIKit

class ViewController: UIViewController, UIColorPickerViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        
        view.addSubview(button)
        button.backgroundColor = .link
        button.setTitle("Select Color", for: .normal)
        button.center = view.center
        button.addTarget(self, action: #selector(didTapSelectColor), for: .touchUpInside)
    }
    
    @objc private func didTapSelectColor(){
        let colorPickerVC = UIColorPickerViewController()
        
        colorPickerVC.delegate = self
        present(colorPickerVC, animated: true, completion: nil)
    }

    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        let color = viewController.selectedColor
        view.backgroundColor = color
    }
    
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        let color = viewController.selectedColor
//        view.backgroundColor = color
    }

}

